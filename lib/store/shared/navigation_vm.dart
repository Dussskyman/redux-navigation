import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_navigation/store/application/app_state.dart';
import 'package:redux_navigation/store/shared/navigation_selector.dart';

class NavigationVM {
  final void Function(String route) changePage;

  NavigationVM({@required this.changePage});

  static NavigationVM fromStore(Store<AppState> store) {
    return NavigationVM(changePage: NavigationSelector.navigateAction(store));
  }
}
