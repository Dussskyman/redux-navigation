import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_navigation/store/application/app_state.dart';

class NavigationSelector {
  static void Function(String route) navigateAction(Store<AppState> store) {
    return (route) => store.dispatch(NavigateToAction.replace(route));
  }
}
