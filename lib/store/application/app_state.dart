class AppState {
  AppState();

  factory AppState.initial() {
    return AppState();
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState();
  }
}
