import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_navigation/res/routes.dart';
import 'package:redux_navigation/store/application/app_state.dart';
import 'package:redux_navigation/store/shared/navigation_vm.dart';

class NavigationPanel extends StatelessWidget {
  const NavigationPanel({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, NavigationVM>(
      converter: NavigationVM.fromStore,
      builder: (context, vm) => Material(
        color: Colors.transparent,
        child: SizedBox(
          width: double.infinity,
          height: 400,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap: () => vm.changePage(Routes.main),
                child: Container(
                  height: 75,
                  color: Colors.amber,
                ),
              ),
              InkWell(
                onTap: () => vm.changePage(Routes.first),
                child: Container(
                  height: 75,
                  color: Colors.red,
                ),
              ),
              InkWell(
                onTap: () => vm.changePage(Routes.second),
                child: Container(
                  height: 75,
                  color: Colors.purple,
                ),
              ),
              InkWell(
                onTap: () => vm.changePage(Routes.third),
                child: Container(
                  height: 75,
                  color: Colors.green,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
