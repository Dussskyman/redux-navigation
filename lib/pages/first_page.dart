import 'package:flutter/material.dart';
import 'package:redux_navigation/navigator_panel.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      drawer: Drawer(
        child: NavigationPanel(),
      ),
    );
  }
}
