import 'package:flutter/material.dart';
import 'package:redux_navigation/navigator_panel.dart';

class ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green,
      drawer: Drawer(
        child: NavigationPanel(),
      ),
    );
  }
}
