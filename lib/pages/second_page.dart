import 'package:flutter/material.dart';
import 'package:redux_navigation/navigator_panel.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple,
      drawer: Drawer(
        child: NavigationPanel(),
      ),
    );
  }
}
