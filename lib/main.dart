import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_navigation/navigator_panel.dart';
import 'package:redux_navigation/pages/first_page.dart';
import 'package:redux_navigation/pages/second_page.dart';
import 'package:redux_navigation/pages/third_page.dart';
import 'package:redux_navigation/res/routes.dart';
import 'package:redux_navigation/store/application/app_state.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({Key key, this.store}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        navigatorKey: NavigatorHolder.navigatorKey,
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case (Routes.main):
              return MaterialPageRoute(
                builder: (context) => MyHomePage(),
              );
            case (Routes.first):
              return MaterialPageRoute(
                builder: (context) => FirstPage(),
              );
            case (Routes.second):
              return MaterialPageRoute(
                builder: (context) => SecondPage(),
              );
            case (Routes.third):
              return MaterialPageRoute(
                builder: (context) => ThirdPage(),
              );
          }
        },
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      drawer: Drawer(
        child: NavigationPanel(),
      ),
    );
  }
}
